<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <!-- Styles -->
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
    </head>
    <body>
        <div class="bg">
            <div class="container">
                <div class="row">
                    <h1>Agenda</h1>
                </div>
                <div class="row">
                    <h2 class="content">TENHA ACESSO À SUA AGENDA<br>DE QUALQUER LUGAR!</h2>
                </div>
                @if(Route::has('login'))
                    @if(Auth::check())
                    <div class="row" style="margin-top: 50px;">
                        <a href="{{ url('/admin') }}" class="btnm">ENTRAR</a>
                        <a href="{{ url('/logout') }}" class="btnm" onclick="event.preventDefault();document.getElementById('logout-form').submit();">SAIR</a>
                    </div>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                    @else
                    <div class="row" style="margin-top: 50px;">
                        <a href="{{ url('/login') }}" class="btnm">ENTRE JÁ</a>
                        <a href="{{ url('/register') }}" class="btnm">CADASTRE-SE</a>
                    </div>
                    @endif
                @endif
            </div>
        </div>
    </body>
</html>
