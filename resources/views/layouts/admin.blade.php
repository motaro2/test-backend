<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<title>Painel Administrativo</title>

		<!-- Styles -->
		<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
		<link href="{{ asset('css/styleAdmin.css') }}" rel="stylesheet">
	</head>
	<body>
		<div id="admin">
			<header>          
				<div class="col-md-2 col-xs-2 logo">
					<div class="container name"><h1>AGENDA</h1></div>
					<div class="hidden" onclick="toggleMenu()"><span class="glyphicon glyphicon-menu-hamburger"></span></div>
				</div>
				<div class="col-md-10 col-xs-4 menuUp">
					<div class="">
						<h3>@yield('name')</h3>
					</div>		
				</div>
			</header>
			<div class="col-md-2 col-xs-12 menuSide" id="sideMenu">
				<div class="">
					<ul>
						<li class=""><span class="glyphicon glyphicon-dashboard"></span><a href="{{ url('/admin') }}">Home</a></li>
						<li class=""><span class="glyphicon glyphicon-dashboard"></span><a href="{{ url('/admin/contatos') }}">Contatos</a></li>
						<li><span class="glyphicon glyphicon-off"></span><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Sair</a></li>
					</ul>
				</div>
			</div>
			<div class="col-md-10 col-xs-12 content">
				<div class="">
					@yield('content')
				</div>
			</div>
			<div class="col-md-12 col-xs-12 footer">
				<p>Desenvolvido por Igor Souza. Todos os direitos reservados.</p>
			</div>
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
			<script src="js/bootstrap.min.js"></script>
		</div>
		<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
				{{ csrf_field() }}
		</form>
		<!-- Scripts -->
		<script src="{{ asset('js/app.js') }}"></script>
		<script type="text/javascript">
			var x = document.getElementById('sideMenu');
			window.addEventListener("resize", showMenuIfResize);
			function toggleMenu() {
		      if (x.style.display === 'block') {
		          x.style.display = 'none';
		      } else {
		          x.style.display = 'block';
		      }
			}
			function showMenuIfResize(){
				var w = window.innerWidth;
				if (w >= 992){
	          		x.style.display = 'block';
				}
				else{
	          		x.style.display = 'none';
				}
			}
		</script>	
	</body>
</html>
