@extends('layouts.admin')

@section('name')
Contatos
@endsection

@section('content')
<div id="criarcontatos">
	<h2>Alterar contato</h2>
		<p>Preencha os dados abaixo para cadastrar um novo contato</p>   
		<div class="col-md-12">
			<form class="form-horizontal pull-right" method="POST" action="{{ url('/admin/contatos/' . $contact->id) }}">
				{{ csrf_field() }}
				{{ method_field('DELETE') }}
				<button type="submit" class="btn btn-danger">Deletar</button>
			</form>
			<br>
			<br>
			<form class="form-horizontal" method="POST" action="{{ url('/admin/contatos/' . $contact->id) }}">
				{{ csrf_field() }}
				{{ method_field('PUT') }}
				<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
					<label for="name">Nome:</label>
					<input type="text" class="form-control" name="name" value="{{ $contact->name }}">
				</div>
				<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
					<label for="email">Email:</label>
					<input type="email" class="form-control" name="email" value="{{ $contact->email }}">
				</div>
				<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
					<label for="tel">Telefone:</label>
					<input type="tel" class="form-control" name="tel" value="{{ $contact->tel }}">
				</div>
				<button type="submit" class="btn btn-success">Alterar</button>
				<a href="{{ url('/admin/contatos') }}">Voltar</a>
			</form>
		</div>
</div>
@endsection