@extends('layouts.admin')

@section('name')
Contatos
@endsection

@section('content')
<div id="criarcontatos">
	<h2>Cadastro de contatos</h2>
		<p>Preencha os dados abaixo para cadastrar um novo contato</p>   
		<div class="col-md-12">
			<form class="form-horizontal" method="POST" action="{{ url('/admin/contatos') }}">
				{{ csrf_field() }}
				<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
					<label for="name">Nome:</label>
					<input type="text" class="form-control" name="name">
					@if ($errors->has('name'))
					    <span class="help-block">
					        <strong>{{ $errors->first('name') }}</strong>
					    </span>
					@endif
				</div>
				<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
					<label for="email">Email:</label>
					<input type="email" class="form-control" name="email">
					@if ($errors->has('email'))
					    <span class="help-block">
					        <strong>{{ $errors->first('email') }}</strong>
					    </span>
					@endif
				</div>
				<div class="form-group {{ $errors->has('tel') ? ' has-error' : '' }}">
					<label for="tel">Telefone:</label>
					<input type="tel" class="form-control" name="tel">
					@if ($errors->has('tel'))
					    <span class="help-block">
					        <strong>{{ $errors->first('tel') }}</strong>
					    </span>
					@endif
				</div>
				<button type="submit" class="btn btn-success">Cadastrar</button>
				<a href="{{ url('/admin/contatos') }}">Voltar</a>
			</form>
		</div>
</div>
@endsection