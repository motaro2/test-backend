<?php
function telFormat($tel){
    if(strlen($tel) == 10){
        $formated = '(' . substr($tel, 0, 2) . ') ' . substr($tel, 2, 4) . '-' . substr($tel, 6);
    }else{
        $formated = '(' . substr($tel, 0, 2) . ') ' . substr($tel, 2, 5) . '-' . substr($tel, 7);
    }
    return $formated;
}
?>
@extends('layouts.admin')

@section('name')
Contatos
@endsection

@section('content')
<div id="cadastro">
	<h2>Gerenciamento de contatos</h2>
	<a href="{{ url('/admin/contatos/create') }}" class="mbtn">Cadastrar novo contato</a>
	<hr>
	@if (Session::has('success'))
	<div class="alert alert-success"> <strong>Sucesso!</strong> {{ Session::pull('success') }}</div>
	@endif
</div>
<div id="listagem">
	<h2>Tabela de contatos</h2>
	<p>Tabela que contém todos os seus contatos</p>            
	<table class="table">
		<thead>
			<tr>
				<th>Nome</th>
				<th>E-mail</th>
				<th>Telefone</th>
				<th>Editar/Excluir</th>
			</tr>
		</thead>
		<tbody>	
			@foreach ($userContacts as $userContact)
			<tr>
			<td>{{ $userContact->name }}</td>
			<td>{{ $userContact->email }}</td>
			<td>{{ telFormat($userContact->tel) }}</td>
			<td><a href="{{ url('/admin/contatos/' . $userContact->id) }}" class="mbtn">X</a></td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
@endsection